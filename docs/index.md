###  **_Bitácora_** de trayecto formativo **EFDIA Especializacion en FAbricacion Digital e Innovacion Abierta**.
***  

### **<span style="color:#296073">ESPECIALIZACION EN FABRICACION DIGITAL E INNOCACION ABIERTA </span>**  
![](images/MP01/MP0107.jpg)


La especializacion es ofrecida por UTEC, co-dirigida y curada por Fab Lab Barcelona en el
Instituto de Arquitectura Avanzada de Cataluña (IAAC).

**Estructura general de la especializacion**: la especialización tiene modalidad híbrida que combina instancias sincrónicas por videoconferencia, trabajo virtual en plataforma con participación en foros y discusiones, talleres en los laboratorios de innovación abierta (prácticos u online) y seminarios con docentes invitados.

[MODULOS TECNICOS](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT01/)  

* MT01 Introducción al diseño web. Herramientas Digitales: competencias y herramientas digitales necesarias para documentar el desarrollo de un proyecto, en este caso mediante un sitio web. Principales temas: gestión de proyectos, herramientas digitales, desarrollo web, html, plataformas digitales, entre otros.  

* MT02 Diseño 2D para Láser y CNC: herramientas para diseñar en 2d. Edición de imágenes, conceptos raster, vector, procesamiento de archivos cad para la generación de código-G compatibles con máquina de corte y grabado láser.  

* MT03 Corte láser controlado por
computadora:  conocimientos básicos de procesamiento de archivos CAD para la generación de código-G compatibles con la máquina de corte, grabado láser y cortadora de vinilo.  

* MT04 Modelado 3D: Introducción a diseño 3D a través de software CAD, Fusion 360, modelado 3D y réplica de objetos a través de impresión 3D.  

* MT05 Impresión y Scaneo 3D:  herramientas para el manejo de tecnologías de fabricación aditiva en impresión 3D.

* MT06 Introducción Arduino / Programación: principios de programación y electrónica aplicada en microcontrola-
dores, utilizando la placa de desarrollo Arduino.

* MT07 Diseño y producción de Circuitos - Milling CNC: introduccion al diseño de circuitos electrónicos mediante software de diseño de
circuitos, manufactura de los mismos mediante el ruteo de placas de cobre utilizando la router CNC de prototipado electrónico.

* MT08 Control numérico computarizado - Router CNC : herramientas para desarrollar y construir modelos de control y prototipos utilizando materiales volumétricos y laminares rígidos aplicando tecnología sustractiva de fresado CNC en dos y tres dimensiones mediante programación en software de Generación de Código -G.  

* MT09 Molding and Casting:  herramientas básicas para el manejo de tecnologías de impresión 3D para la creación de moldes de silicona para diferentes aplicaciones como artesanías, comestibles, creación de piezas.  

[MODULOS DE PROFUNDIZACION](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MP01/)

* MP01 Introducción a la Fabricación digital e Innovación / Proyecto Innovador: contexto actual de la Fabricación Digital y la Innovación Abierta, y
sus aplicaciones en la producción y el diseño. Posibles
ideas para el proyecto final. habilidades de diseño de soluciones basadas en metodologías de
innovación abierta y habilidades de evaluación para la mejor acontinua de los procesos productivos utilizando herramientas de fabricación digital.  

* MP02 Prototipado: herramientas para la validación de un proyecto mediante el prototipado de una idea (pieza fundamental en los procesos de innovación). Conocimientos globales en el uso de las herramientas de fabricación digital y metodologías de innovación abierta, y su aplicación en el proceso de prototipado. Conceptos de design thinking, herramientas para prototipado rápido, procesos para llegar a un producto mínimo viable.

* MP03 Transformación digital: contexto actual de la transformación digital. El fortalecimiento de las capacidades locales en lo que refiere al desarrollo de soluciones tecnológicas, abriendo una puerta a la exportación de servicios de diseño, prototipado
y evaluación de innovaciones de base tecnológica con asistencia de herramientas de fabricación digital, facilitando su articulación productiva con diversas cadenas de valor, (desarrollo de software, logística, construcción, industrias 4.0, bioprocesos, economía
circular, etc.).  

[MODULOS INTENSIVOS](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MI01/)

* MI01 Emprendimieto: estrategias útiles para incorporar exitosamente la innovación impulsada por tecnología, tanto en empresas ya existentes como en nuevos emprendimientos. Comprender el funcionamiento del proceso de innovación, identificar oportunidades y atraer clientes, segmentar el mercado y generar valor,  mapear los pasos prácticos de los problemas organizativos
y legales asociados con la creación de una empresa, insertarse en el ecosistema y apropiarse del valor generado.

* MI02 Diseño computacional:Introducción al diseño computacional por medio de Rhino 7 - Grasshopper. Modelados paramétricos. Estrategia de modelado por medio de algoritmos generativos. Optimización de geometrías. Estrategias de fabricación digital (corte láser, impresión 3d, corte cnc). Generación de archivos de fabricación, codificación y montaje. Automatización de procesos. Generación de herramientas paramétricas customizadas. Herramientas de presentación y documentación gráfica.  

* MI03 Innovación abierta: introduccion a la gestión de la innovación, para promover en forma sistemática la innovación en las empresas, construyendo capacidades en equipos de trabajo para abordar de forma profesional la innovación: desde la generación de ideas hasta la gestión de los proyectos industriales.

MODULO DE ACTIVIDAD INTEGRADORA MODALIDAD PRESENCIAL
Taller integrador intensivo de 3 días de modalidad presencial, donde participarán todos los integrantes de la Especialización (locales y regionales) docentes invitados y docentes de Utec. Esta instancia tiene como objetivo generar un espacio de intercambio para la construcción colaborativa en relación a distintas temáticas de fabricación digital e innovación.

MODULO PROYECTO FINAL
Trabajo final integrador aplicado en un área específica a elección, que deberá estar relacionada a un problema real que pueda solucionarse con los métodos y herramientas aprendidos durante la Especialización. La duración de este proyecto es de aproximadamente 8 semanas y será un trabajo individual o grupal a realizar en equipos de un máximo de 2 integrantes.   

![](images/MP01/MP0100.jpg)

***

((PLATAFORMAS para interactuar en la Especialización:**  
  * [DISCORD](https://discord.com/)
  * [EDU](https://edu.utec.edu.uy/)
  * [GIT](https://efdia20212.gitlab.io/efdia2021/)
  * [Zoom/meet](https://zoom.us/)  


***
**FABLAB BARCELONA**

![](images/MP01/MP0106.jpg)  

Fab Lab Barcelona es el primer laboratorio de fabricación digital de la Unión Europea creado en 2007 a partir del Center for Bits and Atoms (CBA) del Massachusetts Institute of Technology (MIT).  
[FABLAB Barcelona](https://fablabbcn.org/)

![](images/MP01/MP0105.jpg)


FabAcademy](https://fabacademy.org/)

***

**LABORATORIOS**

![](images/MP01/MP0104.jpg)  

Los laboratorios cuentan con la siguiente tecnologia disponible:    
[idea](https://claudia_galanzino.gitlab.io/claudiagalanzino/proyecto/idea/)  
[desa](https://claudia_galanzino.gitlab.io/claudiagalanzino/proyecto/desarrollo//)
* Impresoras 3D de SLA, FDM, SLS.  

* Cortadoras láser y router CNC.  

* CNC milling.  

* Placas programables.    

* Herramientas manuales.    

* Escaner 3D.  

* Elementos de prototipado.    

***
[link a EFDIA 2021](https://efdia20212.gitlab.io/efdia2021/)  
