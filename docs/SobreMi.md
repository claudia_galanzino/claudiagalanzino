---
hide:
    - toc

---

**<span style="color:#296073"> Soy Claudia...Diseñadora Industrial </span>**  

*Es este trayecto ... haciendo la Especialización en Fabricación Digital e Innovación Abierta..**



![](../images/About/About02.jpg)  

 <code> Todo en el camino ha de ser aprendizaje...   

 [Mi linkedin](https://www.linkedin.com/in/claudia-galanzino/)


# **<span style="color:#296073"> # Hola !   </span>**  **<span style="color:orange">   :) </span>**
