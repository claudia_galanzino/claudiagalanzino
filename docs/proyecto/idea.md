---
hide:
    - toc
---


# Idea y Concepto  



![](../images/IDEA/idea13.jpg)  

**Sensaciones a través de luces, sombras, aromas,  sonidos.**  

Surge la idea de trabajar en el área de iluminación,  en la búsqueda de una materialización que permitan emitir luces en movimiento o con la sensación de movimiento, la incorporación de piezas de arte, la inspiración en la naturaleza y la posibilidad de materializar estos conceptos de manera digital a través de la automatización de patrones y formas constructivas.  

* Luces y sombras...

![](../images/IDEA/idea05.jpg)
* Patrones de la naturaleza...  


![](../images/IDEA/idea12.jpg)

* Busqueda de Referentes morfologicos en el **Arte y el Diseño**...  

![](../images/IDEA/idea09.jpg)    
[Jim Hodges](https://en.wikipedia.org/wiki/Jim_Hodges_(artist)  

![](../images/IDEA/idea08.jpg)  
[Kirsti Taiviola](https://www.youtube.com/watch?v=z8TIc3tueBU)  

![](../images/IDEA/idea10.jpg)  
[Henry Segerman](http://www.segerman.org/)  

![](../images/IDEA/idea11.jpg)  
[Peter Buning](http://inventorspot.com/articles/shadow_lights_16694)  

### **<span style="color: #296073"> IMPUTS que se van generando para la idea en los diferentes modulos de EFDIA</span>**  


Si bien todos los conceptos  desarrollados en los módulos de la especialización  van a aportar al proyecto de manera transversal, en este espacio voy rescatando  de cada aprendizaje los  aportes que creo van a ser disparadores de ideas y aportes al desarrollo del proyecto...

***  

**[MP01 Proyecto ](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MP01/)**  

**<span style="color:#71D16E">*Formulacion conceptual de la idea* </span>**     

En este módulo se comenzó a definir un discurso preliminar de idea proyecto ...
![](../images/IDEA/idea06.jpg)
**Sensaciones a través de luces, sombras, aromas,  sonidos.**  

Surge la idea es trabajar en el area de iluminacion en la busqueda de objetos interactivos que permitan emitir luces en movimiento, la incorporacion de piezas de arte grafico digital para encontar nuevas sensaciones resulta en este momento un tema inspirador.  
***


**[MP02 Prototipado ](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MP02/)**  

**<span style="color:#71D16E">*Paleta de colores* </span>**  
En este modulo pude definir una paleta de colores para el proyecto...  
![](../images/MP02/MP0210.jpg)

``#FFFF82`` <span style="color: #FFFF82"> Amarillo MEMBRILLO </span>  
``#F5FTD6`` <span style="color: #F5FTD6"> Amarillo Claro MEMBRILLO </span>  
``#d3e9c4`` <span style="color: #d3e9c4"> Verde MEMBRILLO </span>  
``#080213`` <span style="color: #080213"> Negro MEMBRILLO </span>  
``#296073`` <span style="color: #296073"> GRIS AZULADO MEMBRILLO  </span>**

**<span style="color:#71D16E">*Moodboard* </span>**  
Tambien estuve trabajando en un Moodboard o universo visual buscando que todo comience a *respirar* identidad de marca o de proyecto...  
![](../images/IDEA/idea07.jpg)  
***

**[MP3 Materiales ](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MP03/)**  
**<span style="color:#71D16E">Se exprimenta con variantes de materiales </span>**  

Es importante pensar en el ciclo completo del producto, diseñar tambien su fin de vida.
***


**[MI1 Emprendimiento ](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MI01/)**

**<span style="color:#71D16E">*Lienzo de negocio* </span>**

En este modulo estuve trabajando en el lienzo de negocio sobre el proyecto...  
![](../images/idea2.jpg)

**<span style="color:#71D16E">*Apliacacion movil* </span>**  

Tambien me quedo la inquietud de poder elaborar una app que me permita iteractuar con la iluminacion app que [WHIMSICA](https://whimsical.com/) es una herraamienta que me ha parecido de gran utilidad.  

![](../images/idea/idea01.jpg)
***
**MI02 Diseño Computacional**  

**<span style="color:#71D16E">*Diseño paramétrico-generativo* </span>**  

En el módulo intensivo de Diseño Computacional tuve en cuenta la posibilidad de aplicar Grasshopper, fueron inspirdores los conceptor de tramas irregulares de la naturaleza para lograr un entramado con un equilibrio aparentemente inestable y vivo.  

![](../images/MI02/MI02026.jpg)  

Estuve bosquejando y tratando de bajar a conceptos morfologicos la idea de generar piezas de arte grafico digital para provocar diferentes sensaciones con luces y sombras.

![](../images/MI02/MI02027.jpg)  

![](../images/MI02/MI02034.jpg)  

**<span style="color:#71D16E">*Grasshopper* </span>**  

La tramas en Grasshopper me resultan muy interesantes.
![](../images/MI02/MI02019.jpg)     

![](../images/IDEA/idea04.jpg)

Asi que estoy en ese camino, buscando morfologia gerenerativas...
![](../images/IDEA/idea02.jpg)

![](../images/idea1.jpg)

**<span style="color:#71D16E">*Customizacion de producto* </span>**  
 Desde la web quiero poder personalizar el producto en ciertos paraáetros.  
![](../images/IDEA/idea03.jpg)  

![](../images/MI02/MI02023.jpg)
***

****  
**[MI03 Innovacion Abierta](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT01/)**  
**<span style="color:#71D16E">*..* </span>**  
***  


**[MT01 Diseño Web](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT01/)**  
**<span style="color:#71D16E">*Toda la documentacion se basa en lo aprendido en ete modulo* </span>**
***  
**[MT02 Diseño 2D](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT02/)**  

**<span style="color:#71D16E">Isologotipo </span>**  

En el modulo de Diseño 2D realizaron pruebas de isologotipo...  
![](../images/MT02/MT027.jpg)

**<span style="color:#71D16E">*Montaje grafico de la idea* </span>**  


![](../images/MT02/MT028.jpg)


***  

**[MT03 Corte Laser](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT03/)**  
**<span style="color:#71D16E">*La materializacion con laser ...* </span>**  

![](../images/IDEA/idea14.jpg)
[Puzzle Cell Lamp ](https://vimeo.com/680481078)

***
**[MT04 Modelado 3D](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT04/)**  
**<span style="color:#71D16E">*..* </span>**
***
**[MT05 Impresion 3D](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT05/)**  

**<span style="color:#71D16E">*Relevando trabajos en iluminacion con impresion 3D...* </span>**  


![](../images/IDEA/idea15.jpg)

[FloraformChandelier Nervous ](https://vimeo.com/223465251)

[Nervous System](https://n-e-r-v-o-u-s.com/blog)
***


**[MT06 Arduino](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT06/)**  
**<span style="color:#71D16E">*xx* </span>**
***   
**[MT07 IOT](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT07/)**  
**<span style="color:#71D16E">*xx* </span>**
***   

**[MT08 Circuitos electronicos](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT08/)**  
**<span style="color:#71D16E">*xx* </span>**
***
**[MT09 CNC](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT09/)**  
**<span style="color:#71D16E">*xx* </span>**
![](../images/IDEA/idea16.jpg)
***
**[MT10 Molding ](https://claudia_galanzino.gitlab.io/claudiagalanzino/modulos/MT10/)**  
**<span style="color:#71D16E">*xx* </span>**
***
