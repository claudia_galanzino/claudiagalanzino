---
hide:
    - toc
---

# Desarrollo

Sugerencias 29.04.22.  
* Trabajar con Corte laser para lograr la presición deseada en los calados.  
* Materiales potencialmente posibles: polipropileno negro, madera laminada fina flexible.  
* Se recomenda la utilizacion de Slicer Fusion 360.  
* [Plugins de Grasshopper Pufferfish](https://www.google.com.ar/search?q=pufferfish+patterns+grasshopper&sxsrf=ALiCzsY1uxrtOC6QbEw3PSODqgKryQeNgw:1651261080778&source=lnms&tbm=isch&sa=X&ved=2ahUKEwivrprxgrr3AhVggpUCHThiCTQQ_AUoAXoECAEQAw&biw=1024&bih=488&dpr=1.56)  
* Led o bombilla controlada por relé.  
* Sensor de utrasonido.  
[Ultrasonic sensor max sonar](https://www.google.com.ar/search?q=ultrasonic+sensor+max+sonar&tbm=isch&ved=2ahUKEwj5iriUhrr3AhXYq5UCHfk2BLQQ2-cCegQIABAA&oq=ultrasonic+sensor+max+sonar&gs_lcp=CgNpbWcQAzoHCCMQ7wMQJ1CsBVjzK2CKLmgAcAB4AIABUogB1QSSAQE4mAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=B0JsYrnrOtjX1sQP-e2QoAs&bih=488&biw=1024)  
Un sensor de ultrasonido detecta un rango mas amplio que un sensor laser

https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade

[ultrasonic sensor max sonar](https://www.google.com.ar/search?q=ultrasonic+sensor+max+sonar&tbm=isch&ved=2ahUKEwj5iriUhrr3AhXYq5UCHfk2BLQQ2-cCegQIABAA&oq=ultrasonic+sensor+max+sonar&gs_lcp=CgNpbWcQAzoHCCMQ7wMQJ1CsBVjzK2CKLmgAcAB4AIABUogB1QSSAQE4mAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=B0JsYrnrOtjX1sQP-e2QoAs&bih=488&biw=1024)

[Miro](https://miro.com/app/board/o9J_lxIOIrc=/)  

**BUSQUEDA DE LA FORMA**
* [La compleja geometría del diseño islámico - Eric Broug ](https://www.youtube.com/watch?v=pg1NpMmPv48)  

**DEFINICION DE LA FORMA**
* [La compleja geometría del diseño islámico - Eric Broug ](https://www.youtube.com/watch?v=pg1NpMmPv48)

**DEFINICION DE LA ELECTRONICA**

*Iluminacion sensible al sonido*
* [Tira LED al Ritmo de la Música](https://www.youtube.com/watch?v=ZsMuX1fI0Fs)  
* [Crea tu propio Vumetro](https://www.youtube.com/watch?v=UKPu2W9LM2c)  
* [Arduino. Sensor de Sonido KY-038 y KY-037 (modo digital)](https://www.youtube.com/watch?v=wfiSH0CkInE)
* [Arduino. Sensor de Sonido KY-038 y KY-037 (modo digital)](https://www.youtube.com/embed/HO6xQMR8naw?enablejsapi=1&controls=1&rel=0&showinfo=0&fs=0&autoplay=1&mute=1)  
* [Neopixel anillo](https://articulo.mercadolibre.com.ar/MLA-775121403-neopixel-ring-24-x-5050-rgb-led-with-integrated-drivers-_JM#position=12&search_layout=grid&type=item&tracking_id=e05e7171-4ba7-441c-ac9c-ee7a15ad8aff)  
* [Neopixel anillo](https://articulo.mercadolibre.com.ar/MLA-717962390-anillo-16-leds-rgb-5050-ws2812-neopixel-cjmcu-arduino-nubbeo-_JM?variation=173876057261#reco_item_pos=0&reco_backend=machinalis-v2p-pdp-boost-v2&reco_backend_type=low_level&reco_client=vipM-v2p&reco_id=7126b48c-8e70-4d3d-956f-fee850e12ba4)  
* [Neopixel tira](https://articulo.mercadolibre.com.ar/MLA-914436012-adafruit-dotstar-digital-led-strip-black-144-ledm-05-_JM#position=6&search_layout=grid&type=item&tracking_id=e05e7171-4ba7-441c-ac9c-ee7a15ad8aff)  

Placas que equalizan el sonido y utilizan un conector jag como el de los auriculares.  

* [MSGEQ7 Arduino Tutorial 01: Getting Started](https://rheingoldheavy.com/msgeq7-arduino-tutorial-01-getting-started/)   

* [MSGEQ7 Seven Band Spectrum Analyzer Breakout Board Mono/Stereo (Stereo)](https://www.amazon.com/MSGEQ7-Spectrum-Analyzer-Breakout-Stereo/dp/B08GYCHXYX?th=1)

* [NEOPIXEL con Arduino](https://www.youtube.com/watch?v=vUpl8OMknNg)  



**SELECCION Y COMPRA DE COMPONENTES ELECTRONICOS**  
* [NEOPIXEL](https://www.nubbeo.com.ar/search/?q=NEOPIXEL)  

**PASAJE DE ARDUINO A PCB**  
* [Arduino a PCB ](https://pcbcentral.com/arduino2pcb)  

* [https://ar.mouser.com/ProductDetail/Seeed-Studio/104020109?qs=byeeYqUIh0PmEriTC2u0yQ%3D%3D ](https://ar.mouser.com/ProductDetail/Seeed-Studio/104020109?qs=byeeYqUIh0PmEriTC2u0yQ%3D%3D)  

* [https://www.youtube.com/watch?v=JoU7lTGQ3iU ](https://www.youtube.com/watch?v=JoU7lTGQ3iU)  

* [Sleep mode Arduino ](https://www.prometec.net/el-modo-sleep-en-arduino/)  

**Consultas Clase 17 de mayo 2022**  

* [NeoPixel Controlado Por Un Sensor Ultrasonico   Arduino](https://www.youtube.com/watch?v=ZLH6ow4fuUY)  
* [Circuit design neopixel + ultrasonic | Tinkercad](https://www.tinkercad.com/things/hRP1jdVU8is)  
* [Installing the NeoPixel Library](https://www.youtube.com/watch?v=wmP10z1mvwo)  
* [Adafruit_NeoPixel ejemplos](https://github.com/adafruit/Adafruit_NeoPixel/tree/master/examples)  
* [Tinkercad neopixel + ultrasonic](https://www.tinkercad.com/things/hRP1jdVU8is)
* [Tinkercad](https://www.tinkercad.com/search?category=circuits&sort=relevance&q=ultrasonic%20neopixel%20&view_mode=default)  

 **COMPRA DE COMPONENTES**  
 ![](../images/DESA/DESA01.jpg)
 **SOLDADO DE NEOPIXEL A CABLE Y  ANALISIS DE CODIGO**  
  ![](../images/DESA/DESA02.jpg)
   **PRUEBA DE SOFTWARE: jugando con colores**   
   ![](../images/DESA/DESA03.jpg)  

* [Neopixel LED RGB WS2812](https://www.youtube.com/watch?v=MeTmdw-FTPA)  

   ![](../images/DESA/DESA04.jpg)  
