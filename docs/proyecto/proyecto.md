---
hide:
    - toc
---


# **<span style="color: #296073">PROYECTO INNOVADOR. Propuesta final </span>**
El proyecto final debe incorporar **diseño 2D**, **diseño 3D**, **procesos de fabricacion aditivos y sustractivos**, **electrónica**, y **programación**.

Se deben resptar los siguiente conceptos :  

- <span style="color: orange"> DISEÑO INTEGRADO:</span> Todo lo que forme parte del proyecto debe estar diseñado, las relaciones entre componentes también.  

- <span style="color: orange"> DISEÑO HONESTO: </span>  La tecnología debe estar utilizada de modo funcional al objetivo del proyecto.




### **<span style="color:#71D16E"> ¿Qué hará?</span>**  

### **<span style="color:#71D16E"> ¿A qué proyecto anterior se relaciona?</span>**  
### **<span style="color:#71D16E"> ¿Qué diseñarás?</span>**  
### **<span style="color:#71D16E"> ¿Qué materiales y componentes se utilizarán?(especificar qué materiales ya tienes y cuáles necesitas conseguir)</span>**  
### **<span style="color:#71D16E"> ¿Cuánto costarán aproximadamente?</span>**  
### **<span style="color:#71D16E">  ¿Qué piezas y sistemas se fabricarán?</span>**  
### **<span style="color:#71D16E">¿Qué procesos se utilizarán?</span>**  
* Dibujo 2D.
* Modelado 3D
* Corte laser.  
* Router CNC.  
* Programación.


### **<span style="color:#71D16E">¿Cuántas visitas al laboratorio necesitas?</span>**
En mi espacio de trabajo tengo maquina de corte laser y router CNC, pienso trabajar con talleres locales que me pemitan concretar el proyecto.  

### **<span style="color:#71D16E">¿Qué preguntas necesitan ser respondidas?</span>**           

...
