## Bienvenido a la web personal de Especialización en Fabricación Digital e Innovación

Visitar[UTEC](https://edu.utec.edu.uy/)oara ver las clases u otra iformacion.

* Este sitio se contruye y publica automaticamente utilizando [GitLab CI](https://about.gitlab.com/gitlab-ci/), cada vez que se editan los archivos en la carpeta docs.
* El contenido de markdown se genera usando la herramienta Mkdocs, un generador de sitiosescrito en Python.
* You can start by customizing the file `mkdocs.yml` with your information
  * To change the looks of your website, use the theme options found in the `mkdocs.yml` file or see the names of the available themes
  Para cambiar el aspecto de su sitio web, pueden usarse las los temas que se encuentran en el archivo `mkdocs.yml` o ver los temas disponibles en mkdocs.yml.
* Para ecrear un sitio web desde cero, puede eliminarse todo este repositorio y hacer uno nuevo.

Mas info sobre  MkDocs en [mkdocs.org](http://www.mkdocs.org).


## Building mkdocs locally

To work locally on your computer with this project, you can start with the following the steps:

> Remember to setup your SSH keys to work locally, see [GitLab Docs](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

1. Clone or download this project on your computer
	* Find the clone url at the top of your projects "overview" page
	* Run `git clone git@gitlab.fabcloud.org:your/project/path.git`
1. [Install](http://www.mkdocs.org/#installation) MkDocs on your computer
1. Preview your project: `mkdocs serve`, your site can be accessed under `localhost:8000`
1. To add new pages, create the markdown file in the `docs/` folder (i.e. `touch docs/about.md`)
1. Push your changes to GitLab to automatically publish your changes
	* `git commit -m "Updated site"`
	* `git push master`


#### Git global setup

Do not forget to configure your local git environment, with same details used on Gitlab-Fabcloud.
```
git config user.name "You Name"
git config user.email "you@example.org"
```
